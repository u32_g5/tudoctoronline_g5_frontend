import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';

const Register = () => {

    const[user, setUser] = useState(
        {
        name:"",
        email:"",
        password:"",
        confirm: "",

        }
    );

    const {name, email, password, confirm} = user;

    const changeEvent = (event) =>{
        setUser({
            ...user, 
            [event.target.name]: event.target.value
        }
        );

    }

    const saveUser = async ()=>{
        const body = {
            name:user.name,
            email:user.email,
            password:user.password
        }
        const response= APIInvoke.invokePOST('/user/save',body);
        console.log(response)
    }

    useEffect ( ()=> {
        document.getElementById("name").focus ();
    }, []);

    const onSubmit = (event) => {
        event.preventDefault();
        saveUser();
    }
    return (
        <div className=' hold-transition register-page'>
            
            <div className="register-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <Link to={"#"} className="h1"><b>Tu Doctor </b>Online</Link>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Registre sus datos personales</p>
                        
                        <form onSubmit={onSubmit}>
                            <div className="input-group mb-3">
                                <input type="text" 
                                className="form-control" 
                                placeholder="Full name"
                                id="name"
                                name="name" required
                                value={name}
                                onChange = {changeEvent} />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="email"
                                className="form-control" 
                                placeholder="Email"
                                id="email"
                                name="email" required
                                value={email}
                                onChange = {changeEvent}  />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" 
                                className="form-control" 
                                placeholder="Password"
                                id="password"
                                name="password" required
                                value={password}
                                onChange = {changeEvent}  />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" 
                                className="form-control" 
                                placeholder="Retype password"
                                id="confirm"
                                name="confirm" required
                                value={confirm}
                                onChange = {changeEvent}  />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="social-auth-links text-center">
                                <button type='submit' className="btn btn-block btn-primary">
                                    Registrarse
                                </button>

                                <Link to={"/"} className="btn btn-block btn-danger">
                                    Atrás
                                </Link>
                        </div>
                            
                        </form>

                    </div>
                </div>
            </div>

        </div>
    );

}

export default Register;
